import React, { Component } from "react";
import todosList from "./todos.json";
// import EnterInput from './components/EnterInput'


// function EnterInput (event) {
//   if(event.keyCode === 13){
//       console.log('Input Entered')
//   }
// }
// function CheckOff (event) {
// if(event){
//   console.log('checkedOff')
// }
// }
// function Delete (event) {
//   if(event){
//     console.log('Deleted')
//   }
//   }
function ClearCompleted (event){
  if(event){
    console.log('Cleared Completed')
  }
}

class App extends Component {
  
  state = {
    todos: todosList,
    text: ''
  };
     handleEnterInput = (event)  => {
    if(event.keyCode === 13){
let newTodoItem = event.target.value
let newTodo = {userId: 1, id: Math.random()*10000, title: newTodoItem, completed: false}
this.setState(state =>{
  return {
   todos: [newTodo, ...state.todos],
   text: ''
  }
})
    }
   
    }
    handleChange = event => {
        this.setState({
          text: event.target.value
        });
    };

    handleCheckOff = id => {
      let newTodos =  this.state.todos.map(todo =>{
        if(todo.id=== id){
          return{
            ...todo, completed: !todo.completed
          }
        }
        return {
          ...todo
        }
      }) 
      this.setState({
        todos: newTodos
      })
      }
      
      handleDelete= (todoId) => {
        const newTodos = this.state.todos.filter(
          todoItem => todoItem.id !==todoId
        )
        this.setState({
          todos: newTodos
        })
      }
      handleDeleteCompleted=()=>{
      const newTodos = this.state.todos.filter(
        todoItem => todoItem.completed !==true
      )
      this.setState({
        todos: newTodos
      })

    }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onChange={this.handleChange}
          onKeyDown ={this.handleEnterInput} 
          value= {this.state.text}
          className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus 
          />
        </header>
        <TodoList 
        todos={this.state.todos} 
        handleCheckOff={this.handleCheckOff} 
        handleDelete ={this.handleDelete}
        handleDeleteCompleted ={this.handleDeleteCompleted}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
          onClick={this.handleDeleteCompleted} 
          className="clear-completed"
          >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          onChange={(event)=> this.props.handleCheckOff(this.props.id)} 
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed} />
          <label>{this.props.title}</label>
          <button 
           onClick={(event)=> this.props.handleDelete(this.props.id)} 
          className="destroy" />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            id = {todo.id}
            key= {todo.id}
            title={todo.title} 
            completed={todo.completed} 
            handleCheckOff = {this.props.handleCheckOff}
            handleDelete = {this.props.handleDelete}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
